local PLAYER = FindMetaTable("Player")

Timer = Timer or {}
Timer.Prespeed = 280
Timer.Color = {
	Start = Color(0,255,0),
	End = Color(255,0,0),
}
Timer.Record = {}

hookAdd("Init_Player_Vars", "Init_timer", function(ply)
	ply:AddSettings("status", false)
	ply:AddSettings("timer", 0)
	ply:AddSettings("records", {})

	ply:AddSettings("can_prespeed", false)
	
	for mode, v in pairs(MOVEMENT_MODES) do
		ply:records()[mode] = 9999999
		for sub_mode, v in pairs(MOVEMENT_SUB_MODES) do
			ply:records()[mode .. "_" .. sub_mode] = 9999999
		end
	end
	
end)

function Timer.GetTime(start, last)
	if !isnumber(start) then
		return start
	end
	if !last then
		last = 0
	end
	return string.ToMinutesSecondsMilliseconds( start - last )
end

function Timer.GetPlacement(var)
	if isnumber(var) then
		for k, tbl in pairs(Timer.Record[ply:GetMode()]) do
			if tbl.time >= var then
				return k
			end
		end
	elseif isentity(var) then
		return var:GetPlacement()
	end
end

function Timer.SetRecord(ply, mode, t)
	ply:records()[mode] = t
	
	table.remove(Timer.Record[mode], ply:GetPlacement())
	table.insert(Timer.Record[mode], {
		nick = ply:Nick(),
		sid = ply:SteamID(),
		time = t
	})
	table.sort( Timer.Record[mode], function( a, b ) return a.time > b.time end )
end

function PLAYER:GetTimer()
	return Timer.GetTime(ply:timer())
end

function PLAYER:GetPlacement()
	for k, tbl in pairs(Timer.Record[ply:GetMode()]) do
		if tbl.sid == ply:SteamID() then
			return k
		end
	end
end

function PLAYER:StartTimer()
	if ply:Speed2D() >= Timer.Prespeed then
		chat.Text(ply, ut_col_pal.CADMIUM_RED, GetNotification(Notifications.Client_Messages.Prespeed, ply:Speed2D(), Timer.Prespeed))
		ply:Reset()
		return
	end

	ply:timer(0)
	ply:status("Running")
end

function PLAYER:UpdateRecord()
	local mode = ply:GetMode()
	if ply:records()[mode] <= ply:timer() then
		chat.Text(ply, ut_col_pal.CADMIUM_RED, GetNotification(Notifications.Client_Messages["Finished Run"], ply:GetTimer()))
		return
	end

	if ply:records()[mode] > ply:timer() then
		Timer.SetRecord(ply, mode, ply:timer())
		if ply:timer() > Timer.Record[mode].time then
			chat.Text(ply, ut_col_pal.CADMIUM_RED, GetNotification(Notifications.Client_Messages["Personal Best"], ply:GetTimer()))
		else
			chat.Text(ply, ut_col_pal.CADMIUM_RED, GetNotification(Notifications.Client_Messages["World Record"], ply:GetTimer()))
			
		end
	end
	
end

function PLAYER:StopTimer(update_record)
	ply:status("Ending")
	
	if not update_record then
		chat.Text(ply, ut_col_pal.CADMIUM_RED, GetNotification(Notifications.Client_Messages["Timer Stopped"]))
		return
	end
	ply:UpdateRecord()
end