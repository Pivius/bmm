local PLAYER = FindMetaTable( "Player" )

Zone = Zone or {}

Zone.Zoners = {
	"STEAM_0:1:94755407"
}

Zone.Zones = {}

local template = {
	Start = Vector(0,0,0),
	End = Vector(0,0,0),
	AC = {},
	Checkpoints = {},
	Name = "Untitled"
}

hookAdd("Init_Player_Vars", "Init_Zone", function(ply)
	ply:AddSettings("zoning", {
		Start = false,
		End = false,
		AC = {},
		Checkpoints = {},
		Name = "Untitled",
		Mode = "Feet"
	})
	ply:AddSettings("is_zoning", false)

	
end)

function PLAYER:ToggleZoning()
	ply:is_zoning(not ply:is_zoning())
end

function PLAYER:SetZoningMode(mode)
	mode = mode:lower()
	if mode == "feet" or mode == "head" or mode == "eye" then
		ply:zoning().Mode = mode
	end
end

function PLAYER:SetZoneName(name)
	ply:zoning().Name = name
end


function Zone.StartCreate(ply)

end