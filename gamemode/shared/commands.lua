Commands = {}

function AddCommand(cmd, func)
    if istable(cmd) then
        for _, command in ipairs(cmd) do
            Commands[command] = func
        end
    else
        Commands[cmd] = func
    end
end

AddCommand({"r", "reset", "restart"}, function(ply)
    ply:Reset()
end)



hook.Add("PlayerSay", "Commands", function(ply, txt, team)
    local prefix = txt:sub(1, 1)
	if prefix == "!" or prefix == "/" then
		txt = txt:sub(2)
		local args = string.Split(txt, " ")
        local cmd = string.lower(table.concat( args, " " ))
        if Commands[txt:lower()] then
            Commands[txt:lower()](ply, true)
            return ""
        end
        chat.Text(ply, GetNotification(Notifications.Client_Messages.Unknown, GetPrefix("Error"), txt))
        return ""
	end

end)
