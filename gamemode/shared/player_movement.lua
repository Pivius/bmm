
if CLIENT then
	CreateClientConVar( "mm_mode", "1", false,true, "Switches between modes. 1-Parkour, 2-Grapple, 3-Radioskate, 4-VQ3, 5-CPMA, 6-Bhop, 7-Momentum")
	cvars.AddChangeCallback( "mm_mode", function( convar_name, value_old, value_new )
		LocalPlayer():movement_mode(math.max(math.min(value_new, #MOVEMENT_MODES), 1))
		net.Start("Mode_Networking")
		net.WriteInt(LocalPlayer():movement_mode(), 6)
		net.WriteEntity(LocalPlayer())
		net.SendToServer()
		LocalPlayer():friction(8)
		LocalPlayer():accel(10)
		LocalPlayer():air_accel(10)
		LocalPlayer():gravity(300)
	end )
else
	util.AddNetworkString( "Mode_Networking" )
	
	net.Receive("Mode_Networking", function()
		local int = net.ReadInt(6)
		local ply = net.ReadEntity()
		ply:movement_mode(int)
		ply:friction(8)
		ply:accel(10)
		ply:air_accel(10)
		ply:gravity(300)
	end)
	
	
end
-- TODO Add cliptime for quake physics https://github.com/enneract/cuboid/blob/master/src/game/bg_pmove.c
//
// Player Vars
//
hookAdd("Init_Player_Vars", "Init_Movement", function(ply)
	ply:AddSettings("last_vel", Vector(0,0,0))
	ply:AddSettings("top_speed", {})
	ply:AddSettings("air_gain", 32.8)
	ply:AddSettings("air_accel", 10)
	ply:AddSettings("air_stop", 0)
	ply:AddSettings("air_control", 0)
	ply:AddSettings("strafe_accel", 0)
	ply:AddSettings("wish_speed", 400)
	ply:AddSettings("accel", 10)
	ply:AddSettings("friction", 8)
	ply:AddSettings("stopspeed", 75)
	ply:AddSettings("gravity", 300)
	ply:AddSettings("air_accel_speed", 0)
	ply:AddSettings("water_friction", 1)
	ply:AddSettings("water_accel", 10)
	ply:AddSettings("LeftGround", false)
	ply:AddSettings("movement_mode", "Parkour")
	ply:AddSettings("movement_sub_mode", false)
	ply:AddSettings("keys", table.Copy(MOVEMENT_KEYS))
	ply:AddSettings("velocity_scale", 1)
	ply:AddSettings("default_jump_power", ply:GetJumpPower())
	ply:AddSettings("can_autohop", true)
	ply:AddSettings("air_strafe", "normal")

	for mode, v in pairs(MOVEMENT_MODES) do
		ply:top_speed()[mode] = 0
		for sub_mode, v in pairs(MOVEMENT_SUB_MODES) do
			ply:top_speed()[mode .. "_" .. sub_mode] = 0
		end
	end

	
end)

function SetTopspeed(ply)
	ply = ut_ply.GetPlayer(ply)

	if ply:top_speed()[ply:GetMode()] < ply:Speed2D() then
		ply:top_speed()[ply:GetMode()] = ply:Speed2D()
	end
end

function GetTopspeed(ply)
	ply = ut_ply.GetPlayer(ply)
	
	return ply:top_speed()[ply:GetMode()]
end

/*
	NAME		- SetJumpVelocity
	FUNCTION	- Sets the amount of velocity to gain by jumping
	ARGS		-
		ply - PLayer
		vel - Jump velocity
*/
function SetJumpVelocity(ply, vel)
	if ply:GetJumpPower() != vel * ply:velocity_scale() then
		ply:SetJumpPower(vel * ply:velocity_scale())
		ply:default_jump_power(vel * ply:velocity_scale())
	end
end

/*
	NAME		- SetWalkSpeed
	FUNCTION 	- Sets the movement speed
	ARGS 		-
		ply - Player
		vel - Movement speed
*/
function SetWalkSpeed(ply, vel)
	if ply:GetWalkSpeed() != vel then
		ply:SetWalkSpeed(vel)
		ply:SetRunSpeed( vel )
	end
end

function SetVelocityScale(ply, scale)
	if ply:velocity_scale() != scale || ply:GetJumpPower() != ply:default_jump_power() * scale then
		ply:velocity_scale(scale)
		ply:SetJumpPower(ply:default_jump_power() * scale)
	end
end

function CheckMoveDir(ply, mv)
	mv:SetForwardSpeed(0)
	mv:SetSideSpeed(0)
	if ply:movement_sub_mode() == "HSW" then
		if mv:KeyDown(IN_FORWARD) and (mv:KeyDown(IN_MOVERIGHT) or mv:KeyDown(IN_MOVELEFT)) and not (mv:KeyDown(IN_MOVERIGHT) and mv:KeyDown(IN_MOVELEFT)) then
			mv:SetForwardSpeed(mv:GetForwardSpeed() + 10000)
			if mv:KeyDown(IN_MOVERIGHT) then
				mv:SetSideSpeed(mv:GetSideSpeed() + 10000)
			elseif mv:KeyDown(IN_MOVELEFT) then
				mv:SetSideSpeed(mv:GetSideSpeed() - 10000)
			end
		end
	elseif ply:movement_sub_mode() == "SHSW" then
		if mv:KeyDown(IN_FORWARD) and (mv:KeyDown(IN_MOVERIGHT) or mv:KeyDown(IN_MOVELEFT)) and not (mv:KeyDown(IN_MOVERIGHT) and mv:KeyDown(IN_MOVELEFT)) then
			mv:SetForwardSpeed(mv:GetForwardSpeed() + 10000)
			if mv:KeyDown(IN_MOVERIGHT) then
				mv:SetSideSpeed(mv:GetSideSpeed() + 10000)
			elseif mv:KeyDown(IN_MOVELEFT) then
				mv:SetSideSpeed(mv:GetSideSpeed() - 10000)
			end
		elseif mv:KeyDown(IN_BACK) and (mv:KeyDown(IN_MOVERIGHT) or mv:KeyDown(IN_MOVELEFT)) and not (mv:KeyDown(IN_MOVERIGHT) and mv:KeyDown(IN_MOVELEFT)) then
			mv:SetForwardSpeed(mv:GetForwardSpeed() - 10000)
			if mv:KeyDown(IN_MOVERIGHT) then
				mv:SetSideSpeed(mv:GetSideSpeed() + 10000)
			elseif mv:KeyDown(IN_MOVELEFT) then
				mv:SetSideSpeed(mv:GetSideSpeed() - 10000)
			end
		end
	else
		if mv:KeyDown(IN_FORWARD) then
			mv:SetForwardSpeed(mv:GetForwardSpeed() + 10000)
		end
		if mv:KeyDown(IN_BACK) then
			mv:SetForwardSpeed(mv:GetForwardSpeed() - 10000)
		end
		if mv:KeyDown(IN_MOVERIGHT) then
			mv:SetSideSpeed(mv:GetSideSpeed() + 10000)
		end
		if mv:KeyDown(IN_MOVELEFT) then
			mv:SetSideSpeed(mv:GetSideSpeed() - 10000)
		end
	end
end

hook.Add("SetupMove", "Core_Movement", function(ply, mv, cmd)
	
	if ply:GetMoveType() == MOVETYPE_NOCLIP then return end
	
	if !ply:OnGround() and !ply:LeftGround() then
		hook.Call("PlayerLeftGround", nil, ply, cmd:TickCount())
	else
		
	end
	mv:SetVelocity(mv:GetVelocity() / ply:velocity_scale())
	if ply:WaterLevel() > 1 and !ply:OnGround() then
		WaterMove(ply, mv, ply:accel(), 1)
	end

	ply:air_accel_speed(0)

	if ply:GetGroundEntity() != NULL and not mv:KeyPressed(IN_JUMP) then
		if !(mv:KeyPressed(IN_JUMP) and !mv:KeyWasDown( IN_JUMP )) then -- Allow bhop
			Friction( ply, mv, cmd, ply:friction() )
			Accelerate( ply, mv, cmd, ply:accel() )
		end
	else
		local validKeys = bit.band(mv:GetButtons(), bit.bnot(bit.bxor(bit.bor( unpack(MOVEMENT_KEYS) ), bit.bor(unpack(ply:keys())))))

		mv:SetButtons( validKeys )
		CheckMoveDir(ply, mv)
		AutoHop(ply, mv, ply:can_autohop())

		if ply:air_strafe() == "normal" then
			AirAccelerate( ply, mv, cmd, ply:air_accel(), ply:air_gain() )
		elseif ply:air_strafe() == "quake" then
			PM_AirMove(ply, mv, cmd)
		end
	end
	
	RampSlide.Slide(ply, mv, cmd)
	
	SetTopspeed(ply)
	
	local fLateralStoppingAmount = ply:last_vel():Length2D() - mv:GetVelocity():Length2D()

	ply:last_vel(mv:GetVelocity())
	StartGravity(ply, mv, ply:gravity(), Vector(0,0,-1))
	mv:SetVelocity(mv:GetVelocity() * ply:velocity_scale())
end)

