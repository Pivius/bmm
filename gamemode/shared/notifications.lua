Notifications = Notifications or {}

Notifications.Prefix = {
    ["Timer"] = util.palette.WHITE,
    ["Mode"] = util.palette.BLUE_WHITE_SHOES,
    ["Error"] = util.palette.CRIMSON,
}

Notifications.Client_Messages = {
    ["Unknown"] = '"|1|" is an unknown command!',
    ["Mode Swap"] = "Mode has been changed to |1|",
    ["Mode Error"] = "You're currently playing |1|",
    ["World Record"] = 'You got a WR in |1| (1/|2|)', 
    ["Finished Run"] = 'You finished your run in |1|',
    ["Personal Best"] = 'You beat your personal best in |1| (|2|/|3|)',
    ["Timer Stopped"] = 'Your timer has been stopped.',
    ["Prespeed"] = 'You left the zone too fast. (|1|/|2|)'
}
Notifications.Server_Messages = {
    ["Personal Best"] = '|1| finished their run in |2| (|3|/|4|)',
    ["World Record"] = '|1| got a WR in |2| (|3|/|4|)', 
}

function GetPrefix(prefix)
    if Notifications.Prefix[prefix] then
        return {util.palette.BLACK, "[", Notifications.Prefix[prefix], prefix, util.palette.BLACK, "]", util.palette.WHITE, " "}
    end
    return {util.palette.BLACK, "[", util.palette.WHITE, prefix, util.palette.BLACK, "]", util.palette.WHITE, " "}
end

function GetNotification(message, ...)
    local args = {...}
    local msg = {}
    local strings = 1
    for i = 1, #args do
        if isstring(args[i]) then
            table.insert(msg, string.Replace(message, "|" .. strings .. "|", args[i]))
            strings = strings+1
        elseif IsColor(args[i]) then
            table.insert(msg, args[i])
        elseif istable(args[i]) then
            table.Merge(msg, args[i])
        end
    end
    return unpack(msg)
end