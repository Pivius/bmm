util = util or {}

util.String = {}

function util.String.WrapText( font, string, width )
	local tbl = string.Explode( " ", string )
	local finaltbl = {}
	local text = {}
	local lines = 1
	
	for k, v in pairs(tbl) do
		if !text[lines] then
			text[lines] = {}
		end
		table.insert(text[lines], v)
		surface.SetFont(font)
		local w, h = surface.GetTextSize( string.Implode(" ", text[lines]) )
		if w >= width then
			table.remove(text[lines])
			lines = lines+1
			table.insert(finaltbl, text)
			text[lines] = {}
			table.insert(text[lines], v)
		elseif w < width and v == "//" then
			table.remove(text[lines])
			lines = lines+1
		end
		if k == #tbl and w < width then
			table.insert(finaltbl, text)
		end
	end
	
	--PrintTable(finaltbl)
	return finaltbl[1]
end

util.s = util.String
ut_str = util.String
