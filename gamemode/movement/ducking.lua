Ducking = Ducking or {}
Ducking.DuckSpeed = 0.2
local HullMin = Vector( -16, -16, 0 )
local HullDuck = Vector( 16, 16, 45 )
local HullStand = Vector( 16, 16, 62 )
local ViewDuck = Vector( 0, 0, 47 )
local ViewStand = Vector( 0, 0, 64 )

hookAdd("Init_Player_Vars", "Init Ducking", function(ply)
	local hull_min = Vector( -16, -16, 0 )
	local hull_duck = Vector( 16, 16, 45 )
	local hull_stand = Vector( 16, 16, 62 )

	ply:SetHull( hull_min, hull_stand )
	ply:SetHullDuck( hull_min, hull_duck )
	
	ply:AddSettings("hull_mins", hull_min)
	ply:AddSettings("hull_maxs", hull_stand)
	ply:AddSettings("crouch_boost", false)
end)

function Ducking.UnDuckTrace(pos, endpos, min, max)
	return util.TraceHull {
		start = pos,
		endpos = endpos,
		mins = min,
		maxs = max,
		mask = MASK_PLAYERSOLID_BRUSHONLY
	}
end

function Ducking.DuckMove(ply, mv)
	local origin = mv:GetOrigin()
	local trace_hull = Ducking.UnDuckTrace(origin, origin, ply:hull_mins(), ply:hull_maxs())
	
	if 
		ply:OnGround() and 
		not mv:KeyDown(IN_DUCK) and 
		ply:Crouching() and 
		(ply:KeyDown(IN_JUMP) or 
		ply:KeyReleased(IN_JUMP)) 
	then
		
		if not trace_hull.Hit then
			ply:RemoveFlags(FL_DUCKING)

			ply:crouch_boost(true)
		end
	end
	if ply:last_vel().z > 0 then
		if ply:crouch_boost() and not ply:IsFlagSet( FL_DUCKING ) then

			ply:AddFlags(FL_DUCKING)
			if not trace_hull.Hit then
				ply:crouch_boost(false)
				
				origin.z = trace_hull.HitPos.z + (HullStand.z - HullDuck.z)
				mv:SetOrigin(origin)
			end
		end
	end
end
hook.Add("PlayerTick", "Ducking.DuckMove", Ducking.DuckMove)

local function InstallView( ply )
	if not IsValid( ply ) then return end
	local maxs = ply:Crouching() and HullDuck or HullStand
	local v = ply:Crouching() and ViewDuck or ViewStand
	local offset = ply:Crouching() and ply:GetViewOffsetDucked() or ply:GetViewOffset()

	local tracedata = {}
	local s = ply:GetPos()
	s.z = s.z + maxs.z
	tracedata.start = s
	
	local e = Vector( s.x, s.y, s.z )
	e.z = e.z + (12 - maxs.z)
	e.z = e.z + v.z
	tracedata.endpos = e
	tracedata.filter = player.GetAll()
	tracedata.mask = MASK_PLAYERSOLID
	
	local trace = util.TraceLine( tracedata )
	if trace.Fraction < 1 then
		local est = s.z + trace.Fraction * (e.z - s.z) - ply:GetPos().z - 12
		if not ply:Crouching() then
			offset.z = est
			ply:SetViewOffset( offset )
		else
			offset.z = math.min( offset.z, est )
			ply:SetViewOffsetDucked( offset )
		end
	else
		ply:SetViewOffset( ViewStand )
		ply:SetViewOffsetDucked( ViewDuck )
	end

end
hook.Add( "Move", "InstallView", InstallView )