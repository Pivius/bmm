local PLAYER = FindMetaTable( "Player" )

if SERVER then
	util.AddNetworkString( "MOVEMENT_SET_MODE" )
	util.AddNetworkString( "MOVEMENT_SET_SUB_MODE" )
end

MOVEMENT_MODES = {}
MOVEMENT_SUB_MODES = {}

MOVEMENT_KEYS = {
	IN_FORWARD,
	IN_MOVELEFT,
	IN_BACK,
	IN_MOVERIGHT,
}

function AddMode(mode, commands, settings)
	local cmds = commands
	table.insert(cmds, mode:lower())
	MOVEMENT_MODES[mode] = settings
	
	AddCommand(cmds, function(ply, notify)
		ply:SetMode(mode, true)
	end)
end

function AddSubMode(mode, commands, settings)
	local cmds = commands
	table.insert(cmds, mode:lower())
	MOVEMENT_SUB_MODES[mode] = settings
	AddCommand(cmds, function(ply, notify)
		ply:SetSubMode(mode)
	end)
end

function PLAYER:SetSettings(settings, reset, networked)
	
	if istable(settings) then
		self:movement_mode("Custom")

		for k, v in pairs(MOVEMENT_MODES) do
			if v == settings then
				self:movement_mode(k)
			end
		end

		for k, v in pairs(settings) do
			if self.settings[k] != nil and not isfunction(v) then
				self.settings[k] = v
			elseif isfunction(v) then
				v(self)
			end
		end

		if settings.walk_speed then
			SetWalkSpeed(self, settings.walk_speed)
		end
		
		if settings.jump_vel then
			SetJumpVelocity(self, settings.jump_vel)
		end

		if reset then
			self:Reset()
		end
				
		if SERVER and not networked then
			net.Start("MOVEMENT_SET_MODE")
				net.WriteEntity(self)
				net.WriteString(self:movement_mode())
				net.WriteBool(false)
				net.WriteBool(false)
			net.Broadcast()
		elseif CLIENT and not networked then
			net.Start("MOVEMENT_SET_MODE")
				net.WriteEntity(self)
				net.WriteString(self:movement_mode())
				net.WriteBool(false)
				net.WriteBool(false)
			net.SendToServer()
		end

	end
end

function PLAYER:SetMode(mode, reset_sub, networked, ignore_notification)
	
	if MOVEMENT_MODES[mode] then
		if self:movement_mode() == mode and not self:movement_sub_mode() and not ignore_notification then
			if CLIENT  then
				chat.Text(util.palette.BITTERSWEET_SHIMMER, GetNotification(Notifications.Client_Messages["Mode Error"], GetPrefix("Mode"), mode))
			end
			return
		end

		if reset_sub then
			self:movement_sub_mode(false)
		end

		self:SetSettings(MOVEMENT_MODES[mode], true, true)

		self:Reset()

		if CLIENT and not ignore_notification then
			chat.Text(util.palette.BLEU_DE_FRANCE, GetNotification(Notifications.Client_Messages["Mode Swap"], GetPrefix("Mode"), mode))
		end
	end
end

function PLAYER:SetSubMode(mode, networked, ignore_notification)
	
	if MOVEMENT_SUB_MODES[mode] then
		if self:movement_sub_mode() == mode and not ignore_notification then
			if CLIENT then
				chat.Text(GetNotification(Notifications.Client_Messages["Mode Error"], GetPrefix("Mode"), mode))
			end
			return
		end
		self:SetSettings(MOVEMENT_MODES[self:movement_mode()], true, true)
		self:movement_sub_mode(mode)

		local move_mode = MOVEMENT_SUB_MODES[mode]

		for k, v in pairs(move_mode) do
			if self.settings[k] != nil and not isfunction(move_mode[k]) then
				self.settings[k] = move_mode[k]
			elseif isfunction(move_mode[k]) then
				move_mode[k](self)
			end
		end
		
		if move_mode.walk_speed then
			SetWalkSpeed(self, move_mode.walk_speed)
		end
		
		if move_mode.jump_vel then
			
			SetJumpVelocity(self, move_mode.jump_vel)
		end

		--self:Reset()

		if SERVER and not networked then
			net.Start("MOVEMENT_SET_SUB_MODE")
				net.WriteEntity(self)
				net.WriteString(mode)
				net.WriteBool(ignore_notification)
			net.Broadcast()
		elseif CLIENT and not networked then
			net.Start("MOVEMENT_SET_SUB_MODE")
				net.WriteEntity(self)
				net.WriteString(mode)
				net.WriteBool(ignore_notification)
			net.SendToServer()
		end

		if CLIENT and not ignore_notification then
			chat.Text(util.palette.BLEU_DE_FRANCE, GetNotification(Notifications.Client_Messages["Mode Swap"], GetPrefix("Mode"), mode))
		end
	end
end

function PLAYER:GetMode()
	local mode = self:movement_mode()

	if self:movement_sub_mode() then
		mode = mode .. "_" .. self:movement_sub_mode()
	end
	return mode
end

net.Receive("MOVEMENT_SET_MODE", function()
    local ply = net.ReadEntity()
	local mode = net.ReadString()
	local reset_sub = net.ReadBool()
	local ignore = net.ReadBool()
	ply:SetMode(mode, reset_sub, true, ignore)
end)

net.Receive("MOVEMENT_SET_SUB_MODE", function()
    local ply = net.ReadEntity()
	local mode = net.ReadString()
	local ignore = net.ReadBool()
	
    ply:SetSubMode(mode, true, ignore)
end)
AddMode("Parkour", {"parkour", "p"}, {
	walk_speed = 400,
	jump_vel = 220,
	accel = 10,
	air_gain = 32.8,
	air_accel = 10,
	gravity = 300,
	friction = 8,
	stopspeed = 10,
	water_accel = 10,
	can_rocketjump = false,
	can_autohop = false,
	can_walljump = true,
	can_wallslide = true,
	keys = table.Copy(MOVEMENT_KEYS),
	air_strafe = "normal"
})

AddMode("Bhop", {"b"}, {
	walk_speed = 250,
	jump_vel = 290,
	accel = 5,
	air_gain = 32.8,
	air_accel = 500,
	gravity = 800,
	friction = 4,
	stopspeed = 75,
	water_accel = 10,
	can_rocketjump = false,
	can_autohop = true,
	keys = table.Copy(MOVEMENT_KEYS),
	air_strafe = "normal"
})

AddMode("10 Air Accel", {"10aa"}, {
	walk_speed = 250,
	jump_vel = 290,
	accel = 5,
	air_gain = 32.8,
	air_accel = 10,
	gravity = 800,
	friction = 4,
	stopspeed = 75,
	water_accel = 10,
	can_rocketjump = false,
	can_autohop = true,
	keys = table.Copy(MOVEMENT_KEYS),
	air_strafe = "normal"
})

AddMode("VQ3", {"vanilla quake 3", "quake"}, {
	walk_speed = 320,
	jump_vel = 290,
	accel = 10,
	air_accel = 1,
	air_stop = 1,
	air_control = 0,
	strafe_accel = 1,
	wish_speed = 400,
	gravity = 800,
	friction = 6,
	stopspeed = 100,
	water_accel = 4,
	can_autohop = true,
	keys = table.Copy(MOVEMENT_KEYS),
	air_strafe = "quake"
})

AddMode("CPMA", {"cpm"}, {
	walk_speed = 320,
	jump_vel = 290,
	accel = 15,
	air_accel = 1,
	air_stop = 2.5,
	air_control = 150,
	strafe_accel = 70,
	wish_speed = 30,
	gravity = 800,
	friction = 8,
	stopspeed = 100,
	water_accel = 4,
	can_autohop = true,
	can_rocketjump = false,
	keys = table.Copy(MOVEMENT_KEYS),
	air_strafe = "quake"
})

AddSubMode("W-Only", {"wonly", "w"}, {
	keys = {IN_FORWARD}
})

AddSubMode("A-Only", {"aonly", "a"}, {
	keys = {IN_MOVELEFT}
})

AddSubMode("D-Only", {"donly", "d"}, {
	keys = {IN_MOVERIGHT}
})

AddSubMode("HSW", {"half-sideways", "half sideways"}, {
	keys = table.Copy(MOVEMENT_KEYS)
})

AddSubMode("SHSW", {"half-sideways", "half sideways"}, {
	keys = table.Copy(MOVEMENT_KEYS)
})

AddSubMode("SW", {"sideways"}, {
	keys = {IN_FORWARD, IN_BACK}
})

AddSubMode("Rabbit", {"rab"}, {
	jump_vel = 450
})

AddSubMode("Stamina", {"stam", "s"}, {
	jump_vel = 220
})

AddSubMode("Low Gravity", {"lg", "lowgrav"}, {
	gravity = 500
})

AddSubMode("High Gravity", {"hg", "highgrav"}, {
	gravity = 1200
})

AddSubMode("Easy Scroll", {"ez", "e", "scroll"}, {
	can_autohop = false
})

AddSubMode("Legit", {"l"}, {
	can_autohop = false
})

AddSubMode("Rocketjumping", {"rj", "rocket"}, {
	can_rocketjump = true
})

for mode, v in pairs(MOVEMENT_MODES) do
	Timer.Record[mode] = {}
	for sub_mode, v in pairs(MOVEMENT_SUB_MODES) do
		Timer.Record[mode .. "_" .. sub_mode] = {}
	end
end