function AutoHop(ply, mv, can_auto_hop)
	if !ply:IsOnGround() and can_auto_hop and ply:WaterLevel() <= 1 then
		mv:SetButtons( bit.band( mv:GetButtons(), bit.bnot( IN_JUMP ) ) )
	end
end

function SmoothStep(ply, time)
	local eye_origin = ply:GetViewOffset()
	local flCurrentPlayerZ = ply:GetPos().z
	local flCurrentPlayerViewOffsetZ = ply:GetViewOffset().z
	if not ply.flOldPlayerZ or not ply.flOldPlayerViewOffsetZ then
		
		ply.flOldPlayerZ = 0
		ply.flOldPlayerViewOffsetZ = 0
	end
	if ( ( ply:GetGroundEntity() != NULL && ply:GetGroundEntity():GetMoveType() == MOVETYPE_NONE ) && ( flCurrentPlayerZ != ply.flOldPlayerZ ) && ply.flOldPlayerViewOffsetZ == flCurrentPlayerViewOffsetZ ) then
		local dir = -1
		if flCurrentPlayerZ > ply.flOldPlayerZ then
			dir = 1
		end

		local steptime = time or FrameTime()

		if steptime < 0 then
			steptime = 0
		end

		ply.flOldPlayerZ = ply.flOldPlayerZ + (steptime * 150 * dir)

		local stepSize = 18.0

		if dir > 0 then
			if ply.flOldPlayerZ > flCurrentPlayerZ then
				ply.flOldPlayerZ = flCurrentPlayerZ
			end

			if (flCurrentPlayerZ - ply.flOldPlayerZ) > stepSize then
				ply.flOldPlayerZ = flCurrentPlayerZ - stepSize
			end
		else
			if (ply.flOldPlayerZ < flCurrentPlayerZ) then
				ply.flOldPlayerZ = flCurrentPlayerZ
			end

			if (flCurrentPlayerZ - ply.flOldPlayerZ < -stepSize) then
				ply.flOldPlayerZ = flCurrentPlayerZ + stepSize
			end
		end

		return ply.flOldPlayerZ - flCurrentPlayerZ
	else
		ply.flOldPlayerZ = flCurrentPlayerZ
		ply.flOldPlayerViewOffsetZ = flCurrentPlayerViewOffsetZ
	end
	return 0
end

if CLIENT then
	local function ViewFix( ply, pos, ang, fov, znear, zfar, view )
		local position = view.pos
		local sm = SmoothStep(ply, 0.003)
		local old_pos = ply.flOldPlayerZ + 64
		local angle = ang
		angle.r = 0
		--position.z = math.Clamp(position.z + sm, math.min(position.z, old_pos), math.max(position.z, old_pos))
		--view.pos = position
		--view.ang = angle 
	end
	hookAdd( 'CalcView', 'ViewFix', ViewFix )
end