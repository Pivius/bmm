local math = math
local sqrt = math.sqrt
local pow = math.pow
local cos = math.cos
local sin = math.sin
local pi = math.pi

local Vector2D = {}
Vector2D.__index = Vector2D

function Vector2D.new(x, y)
  local self = setmetatable( {}, Vector2D )
  self.x = x or 0
  self.y = y or 0
  return self
end

function Vector2D:update(x, y)
  self.x = x or self.x
  self.y = y or self.y
end

function Vector2D:size()
  return sqrt( pow( self.x, 2 ) + pow( self.y, 2 ) )
end

function Vector2D:dotProduct(vec2)
  assert( type( vec2 ) == "table", "vec2 must be a table" )
  assert( type( vec2.x ) ~= "nil", "vec2 must have an x value" )
  assert( type( vec2.y ) ~= "nil", "vec2 must have an y value" )
  return ( self.x * vec2.x ) + ( self.y * vec2.y )
end

function Vector2D:angle(vec2)
  assert( type( vec2 ) == "table", "vec2 must be a table" )
  assert( type( vec2.x ) ~= "nil", "vec2 must have an x value" )
  assert( type( vec2.y ) ~= "nil", "vec2 must have an y value" )
  return math.acos( self:dotProduct( vec2 ) / ( self:size() * vec2:size() ) )
end

local function posAngle(angle)
  -- angle = math.modf(angle, 360) -- this causes bad jitter
  if ( angle < 0 ) then angle = angle + 360 end
  return angle
end

local accel, prev_ang, timer
local player_speed = Vector2D.new( 0, 0 )
local  delta_speed = Vector2D.new( 0, 0 )
local player_accel = Vector2D.new( 0, 0 )

local accelAvg = {
      ["totalAccel"]   = 0,
      ["Samples"]      = 0,
      ["averageAccel"] = 0
}

function GetCGaz(ply, radiusC, lineScaleL, invertC, drawAccelLine )
  local       vel = ply:GetVelocity()
  local   eye_ang = ply:EyeAngles().y
  local accel_dir = 0
  local backwards = false

  if eye_ang > 0 then
    eye_ang = 360 - eye_ang
  else
    eye_ang = 360 + math.abs(eye_ang)
  end

  if ( vel:GetNormalized() * Vector( 1, 1, 0 ) ):Dot( ply:EyeAngles():Forward() ) < 0 then
    backwards = true
  end

  if prev_ang == nil then prev_ang = eye_ang end
  if accel == nil then accel = 0 end

  accel_dir = vel:Length2D() - player_speed:size()
  delta_speed:update( vel.x - player_speed.x, vel.y - player_speed.y )
  player_accel:update( delta_speed.x / 1000, delta_speed.y / 1000 )
  accel = player_accel:size()

  if player_accel.x < 0 and player_accel.y < 0 then
    accel = accel * -1
  end

  player_speed:update( vel.x, vel.y )

  --accel = ply.accelTest

  local  vec_x = Vector2D.new( 1, 0 )
  local vec_nx = Vector2D.new( -1, 0 )
  local pl_ang = posAngle( eye_ang - 180 )
  local vel_ang

  if player_speed.y >= 0 then
    vel_ang = player_speed:angle( vec_nx )
  else
    vel_ang = pi + player_speed:angle( vec_x )
  end

  if backwards then
    vel_ang = -vel_ang
    pl_ang = -pl_ang
  end

  if ply:KeyDown( IN_BACK ) and !( ply:KeyDown( IN_FORWARD ) or ply:KeyDown( IN_MOVELEFT ) or ply:KeyDown( IN_MOVERIGHT ) ) then
    --vel_ang = vel_ang
    pl_ang = pl_ang + 180
  elseif ply:KeyDown( IN_MOVELEFT ) and !( ply:KeyDown( IN_FORWARD ) or ply:KeyDown( IN_BACK ) ) then
    if backwards then
      vel_ang = vel_ang
      pl_ang = pl_ang - 45
    else
      vel_ang = vel_ang + math.rad( 90 )
      pl_ang = pl_ang + 45
    end
  elseif ply:KeyDown( IN_MOVERIGHT ) and !( ply:KeyDown( IN_FORWARD ) or ply:KeyDown( IN_BACK ) ) then
    if backwards then
      vel_ang = vel_ang --math.rad(45)
      pl_ang = pl_ang + 45
    else
      vel_ang = vel_ang - math.rad( 90 )
      pl_ang = pl_ang - 45
    end
  elseif ply:KeyDown( IN_MOVELEFT ) and ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_BACK ) then
    if backwards then

      vel_ang = vel_ang - math.rad( 90 )
      pl_ang = pl_ang + 180
    end

  elseif ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_FORWARD ) and !ply:KeyDown( IN_BACK ) then
    if backwards then
      vel_ang = vel_ang + math.rad( 90 )
      pl_ang = pl_ang - 180
    end
  elseif ply:KeyDown( IN_MOVELEFT ) and ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_FORWARD ) then
    if !backwards then

      vel_ang = vel_ang - math.rad( 90 )
      pl_ang = pl_ang + 180
    end

  elseif ply:KeyDown( IN_MOVERIGHT ) and ply:KeyDown( IN_BACK ) and !ply:KeyDown( IN_FORWARD ) then
    if !backwards then
      vel_ang = vel_ang + math.rad( 90 )
      pl_ang = pl_ang - 180
    end
  end

  local wishspeed = ply.wishspeedTest or 320
  if ( ply.movementDir == 2 || ply.movementDir == 6 ) then
    --wishspeed = ply:WishSpeed()
  end
  local min_ang = math.asin( ( wishspeed - accel ) / player_speed:size() )
  local opt_ang = math.acos( ( wishspeed - accel ) / player_speed:size() )
  local o = math.atan(
  ( accel * sqrt( pow( player_speed:size(), 2 ) - pow( wishspeed - accel, 2 ) ) ) /
  ( pow( player_speed:size(), 2 ) + accel * ( wishspeed - accel ) ) )
  local          a = o / 2 + opt_ang
  local          e = a - pi / 4
  local      min_a = o / 2 + min_ang
  local      min_e = min_a - pi / 4
  local  t_ang_min = vel_ang
  local t_ang_op_m = vel_ang
  local   t_ang_op = vel_ang
  local      test1 = vel_ang
  local      test2 = vel_ang

  if ply:KeyDown( IN_MOVERIGHT ) then
    t_ang_op_m = t_ang_op + 1 * e
    t_ang_op = t_ang_op + 1.2 * e
    test1 = test1 + 1 * min_e
    test2 = test2 + 1.2 * min_e
  elseif ply:KeyDown( IN_MOVELEFT ) then
    t_ang_op_m = t_ang_op - 1 * e
    t_ang_op = t_ang_op - 1.2 * e
    test1 = test1 - 1 * min_e
    test2 = test2 - 1.2 * min_e
  elseif ply:KeyDown( IN_BACK ) then
    t_ang_op_m = t_ang_op - 1 * e
    t_ang_op = t_ang_op - 1.2 * e
    test1 = test1 - 1 * min_e
    test2 = test2 - 1.2 * min_e
  end
    prev_ang = eye_ang


  local ang_diff_min   = t_ang_min - math.rad( pl_ang )
  local ang_diff_op_m  = t_ang_op_m - math.rad( pl_ang )
  local ang_diff_op    = t_ang_op - math.rad( pl_ang )
  local ang_diff_test1 = test1 - math.rad( pl_ang )
  local ang_diff_test2 = test2 - math.rad( pl_ang )

  local radiusC = radiusC or 100
  local NVG_CW = 2
  local NVG_CCW = 1
  local lineScaleL = lineScaleL or 1
  local lineScaleC = 1
  --local invertC = false
  --local drawAccelLine = true
  local lineH = 10
  local lineHOff = 7

  local lineSize = radiusC
  local dir = NVG_CW
  if ang_diff_min < ang_diff_op then dir = NVG_CW else dir = NVG_CCW end

  if invertC then
    if dir == NVG_CW then dir = NVG_CCW
    else dir = NVG_CW end

    ang_diff_min = ang_diff_min + math.rad( 180 )
    ang_diff_op_m = ang_diff_op_m + math.rad( 80 )
    ang_diff_op = ang_diff_op + math.rad( 180 )
  end

  local cgazA1 = ( radiusC * cos( ang_diff_op_m  - pi / 2 ) ) * lineScaleL
  local cgazA2 = ( radiusC * cos( ang_diff_op    - pi / 2 ) ) * lineScaleL
  local cgazB1 = ( radiusC * cos( ang_diff_min   - pi / 2 ) ) * lineScaleL
  local cgazB2 = ( radiusC * cos( ang_diff_op    - pi / 2 ) ) * lineScaleL
  local cgazC1 = ( radiusC * cos( ang_diff_test1 - pi / 2 ) ) * lineScaleL
  local cgazC2 = ( radiusC * cos( ang_diff_test2 - pi / 2 ) ) * lineScaleL
  local cgazD1 = ( radiusC * sin( ang_diff_test1 - pi / 2 ) ) * lineScaleL
  local cgazD2 = ( radiusC * sin( ang_diff_test2 - pi / 2 ) ) * lineScaleL

  local guideCircleWidth = 6
  local guideLineWidth = 4

  ang_diff_min = ang_diff_min * lineScaleC
  ang_diff_op_m = ang_diff_op_m * lineScaleC
  ang_diff_op = ang_diff_op * lineScaleC

  local maxWidth = 0
  local maxPos = 0

  local optWidth = 0
  local optPos = 0

  local rWidth = 0
  local rPos = 0
  if drawAccelLine then
    if ( ply:KeyDown(IN_MOVELEFT) and !backwards ) or  ( ply:KeyDown(IN_MOVERIGHT) and backwards) then
      maxWidth = math.abs(math.min(cgazC1-cgazC2, 0))
      maxPos = cgazA1-maxWidth

      optWidth = math.abs(math.max(cgazA1-cgazA2, 0))-1
      optPos = cgazA1-optWidth-1

      rWidth = math.abs(math.max(cgazD1,cgazD2))-1
      rPos = cgazA1-rWidth-1
    elseif ( ply:KeyDown(IN_MOVERIGHT) and !backwards) or  ( ply:KeyDown(IN_MOVELEFT) and backwards)  then
      maxWidth = math.abs(math.max(cgazC1-cgazC2, 0))
      maxPos = cgazA1

      optWidth = math.abs(math.min(cgazA1-cgazA2, 0))
      optPos = cgazA1

      rWidth = math.abs(math.min(cgazD1,cgazD2))
      rPos = cgazA1
    end
    if ut_math.IsNan(maxWidth, maxPos, optWidth, optPos, rWidth, rPos) then
      maxWidth = 0
      maxPos = 0
      optWidth = 0
      optPos = 0
      rWidth = 0
      rPos = 0
    end
    return maxWidth, maxPos, optWidth, optPos, rWidth, rPos
  end
end

local ease_module = load.Module( "modules/ease.lua" )
ease_cgaz = {}
ease_cgaz.FadeTime = 0
ease_cgaz.Alpha = 0
ease_cgaz.W = 0
ease_cgaz.Pos = 0
ease_cgaz.Ease = ease_module.new(1, {alpha = 0, W = 0, Pos = 0}, {alpha = ease_cgaz.Alpha, W = ease_cgaz.W, Pos = ease_cgaz.Pos}, "inOutCubic")

local cGazR = ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")
local cGazL = ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")
local cGazB = ease_module.new(1, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, {optWidth = 0, optPos = 0, maxWidth = 0, maxPos = 0, rWidth = 0, rPos = 0}, "outInCubic")
ticks_on_ground = 0
--[[
function HUD_CGAZ:Draw3D(ply, scrw, scrh)

  local localPly = Spectate.IsSpectating(ply) or ply
  local x, y     = -500, 0     // X and Y position
  local w, h     = 0, 0            // Width and Height
  local bar_width     = 1000
  local bar_height    = 20
  local alpha = 255
  local color = ColorAlpha(Color(255,255,255), alpha)
  local draw_line = true
  local a = ease_cgaz.Ease:update(1)


  local maxWidth, maxPos, optWidth, optPos, rWidth, rPos = GetCGaz(localPly, 100, 5, false, draw_line)
  
  if localPly:OnGround() then
    if ticks_on_ground > 5 then

      if ease_cgaz.FadeTime <= CurTime() then

        ease_cgaz.W = 0
        ease_cgaz.Alpha = 0
        ease_cgaz.Pos = 0
      end
    else
      ticks_on_ground = ticks_on_ground + 1
    end
  else
    ease_cgaz.FadeTime = CurTime()+2.5
    if ease_cgaz.FadeTime > CurTime() then
      ease_cgaz.W = bar_width
      ease_cgaz.Alpha = alpha
      ease_cgaz.Pos = x
    end
    ticks_on_ground = 0
  end
  ease_cgaz.Ease:Target({alpha = ease_cgaz.Alpha, W = ease_cgaz.W, Pos = ease_cgaz.Pos})
  bar_width = ease_cgaz.Ease:get().W
  print(bar_width)
  x = ease_cgaz.Ease:get().Pos
  alpha = ease_cgaz.Ease:get().alpha
  color = ColorAlpha(Color(255,255,255), alpha)

  surface.SetDrawColor( Color(0,0,0,math.Clamp(alpha, 0, 100)) )
  surface.DrawRect( x, y+(h/2)+(bar_height/2), bar_width, bar_height )

  if draw_line && !(localPly:KeyDown(IN_FORWARD) && localPly:KeyDown(IN_MOVERIGHT) && localPly:KeyDown(IN_MOVELEFT) ) && ticks_on_ground <= 5 then
    local cGazRUpdate = cGazR:update(0.1)
    local cGazLUpdate = cGazL:update(0.1)
    local cGazBUpdate = cGazB:update(0.1)
    rPos = math.Clamp(rPos, x, x+bar_width)
    rWidth = math.Clamp(rWidth, 0, rWidth - ((rPos+  rWidth) - (bar_width/2)))

    optPos = math.Clamp(optPos, x, x+bar_width)
    optWidth = math.Clamp(optWidth, 0, optWidth - ((optPos+  optWidth) - (bar_width/2)))

    maxPos = math.Clamp(maxPos, x, x+bar_width)
    maxWidth = math.Clamp(maxWidth, 0, maxWidth - ((maxPos+  maxWidth) - (bar_width/2)))

    local cgaz_maxWidth = maxWidth
    local cgaz_maxPos = maxPos

    local cgaz_optWidth = optWidth
    local cgaz_optPos = optPos

    local cgaz_rWidth = rWidth
    local cgaz_rPos = rPos

    if ( localPly:KeyDown(IN_MOVERIGHT) && !backwards) || ( localPly:KeyDown(IN_MOVELEFT) && backwards )  then
      local cgaz_maxWidth = cGazR:get().maxWidth
      local cgaz_maxPos = cGazR:get().maxPos

      local cgaz_optWidth = cGazR:get().optWidth
      local cgaz_optPos = cGazR:get().optPos

      local cgaz_rWidth = cGazR:get().rWidth
      local cgaz_rPos = cGazR:get().rPos
      cGazR:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})
    elseif ( localPly:KeyDown(IN_MOVELEFT) && !backwards ) || ( localPly:KeyDown(IN_MOVERIGHT) && backwards ) then
      local cgaz_maxWidth = cGazL:get().maxWidth
      local cgaz_maxPos = cGazL:get().maxPos

      local cgaz_optWidth = cGazL:get().optWidth
      local cgaz_optPos = cGazL:get().optPos

      local cgaz_rWidth = cGazL:get().rWidth
      local cgaz_rPos = cGazL:get().rPos
      cGazL:Target({optWidth = optWidth, optPos = optPos, maxWidth = maxWidth, maxPos = maxPos, rWidth = rWidth, rPos = rPos})
    end

    // Accel Range
    surface.SetDrawColor(Color(240, 211, 20, alpha) )
    surface.DrawRect( x+( bar_width / 2 ) + cgaz_rPos, y+(h)+(bar_height/2), cgaz_rWidth, bar_height-10)
    // Optimal accel
    surface.SetDrawColor(Color(40, 231, 9, alpha) )
    surface.DrawRect( x+( bar_width / 2 ) + cgaz_optPos, y+(h)+(bar_height/2), cgaz_optWidth, bar_height-10)
    // Max accel
    surface.SetDrawColor( Color(32, 95, 201, alpha) )
    surface.DrawRect( x+( bar_width / 2 ) + cgaz_maxPos, y+(h)+(bar_height/2), cgaz_maxWidth, bar_height-10)
  end

end]]