local hud = {}
hud.HUD_CACHE = hud.HUD_CACHE or {}

-------------------------------------
-- CVars.
-------------------------------------
local LagSens = CreateClientConVar( "hud_lag_sensitivity", "10", true, true, "Hud lag sensitivity. Default: 10")

cvars.AddChangeCallback( "hud_lag_sensitivity", function( convar_name, value_old, value_new )
	if tonumber(value_new) > 30 then
		LagSens:SetInt(30)
		return
	elseif tonumber(value_new) < 1 then
		LagSens:SetInt(1)
		return
	end
	local ls = math.Clamp(LagSens:GetInt(),1,30)
	for k, v in pairs(hud.HUD_CACHE) do
		v.tiltRate = math.Clamp(ls,1,30)/4
		v.resetRate = 40/((ls/4)/2)
	end
end )

-------------------------------------
-- INIT UI.
-------------------------------------
function hud.Init()
	local ls = math.Clamp(LagSens:GetFloat(),1,30)

	if hud.HUD_CACHE["HUD_HP"] then
		HUD_HP:Remove()
		hud.HUD_CACHE["HUD_HP"] = nil
	end
	HUD_HP = hudmod.CreatePanel( nil, ls / 4, 40 / ( ( ls / 4 ) / 2 ), -20 )
	hud.HUD_CACHE["HUD_HP"] = HUD_HP
	HUD_HP.moveUpdate = true

	if hud.HUD_CACHE["HUD_VEL"] then
		HUD_VEL:Remove()
		hud.HUD_CACHE["HUD_VEL"] = nil
	end
	HUD_VEL = hudmod.CreatePanel( nil, ls / 4, 40 / ( ( ls / 4 ) / 2 ), 20 )
	hud.HUD_CACHE["HUD_VEL"] = HUD_VEL
	HUD_VEL.moveUpdate = true

	if hud.HUD_CACHE["HUD_KE"] then
		HUD_KE:Remove()
		hud.HUD_CACHE["HUD_KE"] = nil
	end
	HUD_KE = hudmod.CreatePanel( nil, ls / 4, 40 / ( ( ls / 4 ) / 2 ) )

	hud.HUD_CACHE["HUD_KE"] = HUD_KE
end
hud.Init()

-------------------------------------
-- Removes standard UI.
-------------------------------------
function GM:HUDDrawTargetID()
end
function GM:HUDShouldDraw(name)
		local draw = true
		if(name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudSecondaryAmmo" --[[or name == "CHudCrosshair"]] ) then
		draw = false;
		end
return draw;
end

local a = 0 //avg
local s = 0 //samples
local calls = 1000 //calls
local benchmark = false

function HUD_VEL:Draw3D(ply, scrw, scrh, cx, cy)
	local x, y = 0, 0

	if IsValid(Selection) then
		x, y = Selection.Frames.LeftFrame:GetPos()
		x = x + 250
	end

	presets.load("vel_generic_1", ply, scrw, scrh, self, x, cy)
end

function HUD_HP:Draw3D(ply, scrw, scrh, cx, cy)
	presets.load("hp_generic_1", ply, scrw, scrh, self, cx, cy)
end

function HUD_KE:Draw3D(ply, scrw, scrh, cx, cy)
	presets.load("keyboard_echoes", ply, 0, scrh *1.25, self, cx, cy)
end


function GM:OnContextMenuOpen()
	showinfo = !showinfo
end