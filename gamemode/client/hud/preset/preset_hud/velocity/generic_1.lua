/*
	Creator     - Pivius/Moose
	Preset      - vel_generic_1
	Description -
		A HUD style by moose.
		Goes in style with keyboard_echoes preset.
	Benchmark in 1000 calls   -
		Doing nothing -
		Healing       -
*/

local global_vars = {}
local vel_color = util.palette.CAPRI
local base_color = util.palette.LIGHT_GREY
local dark = false

if dark then
	base_color = util.palette.DARK_GREY
end

local ease_module = load.Module( "modules/ease.lua" )
local easing = {a = 0, vel_w = 0}
local VEL_ease = ease_module.new(1, table.Copy(easing), table.Copy(easing), "outCubic")
local VEL_fade_ease = ease_module.new(1, {vel_w = 0}, {vel_w = 0}, "outCubic")
local VEL_bar = Material("openmove/UI/HP/Health_Bar.png", "noclamp")
local VEL_bar_drain = Material("openmove/UI/HP/Health_Bar_drain.png", "noclamp")

local function SpeedDisplay(ply, scrw, scrh, self, cx)
	local localPly = Spectate.IsSpectating(ply) or ply
	local w, h     = 300, 5                      // Width and Height
	local x, y     = -(w/2) - (scrw/2) + cx, scrh - 300      // X and Y position
	local vel      = localPly:Speed2D()
	local alpha    = VEL_ease:get().a
	local color    = ColorAlpha(vel_color, alpha)
	local shade    = Color(base_color.r/1.5, base_color.g/1.5, base_color.b/1.5)
	local shadow    = Color(shade.r/1.5, shade.g/1.5, shade.b/1.5)
	local vel_width = vel*(w/math.max(GetTopspeed(localPly), 100))
	local thickness = 25
	local gap = 2

	//Easing
	VEL_ease:update(2.25)
	VEL_ease:Target({["a"] = 255, ["vel_w"] = vel_width}, true)

	if VEL_fade_ease:get().vel_w <= VEL_ease:get().vel_w then
		VEL_fade_ease:get().vel_w = vel_width
	end

	VEL_fade_ease:update(1.5)
	VEL_fade_ease:Target({["vel_w"] = vel_width}, true)

	// Draws the boxes
	draw_lib.DrawRoundedBox(x, y + (h / 2), w - gap, thickness, ColorAlpha(base_color, math.max(alpha-10, 0)), 0, true, false, false, true)
	draw_lib.DrawRoundedBox(x + gap, y + (h / 2) + gap, w , thickness, ColorAlpha(shadow, math.max(alpha-10, 0)), 0, true, false, false, true)
	draw_lib.DrawRoundedBox(x + gap, y + (h / 2) + gap, math.min(VEL_fade_ease:get().vel_w, w), thickness, ColorAlpha(color, math.Clamp(color.a - 200, 0 ,255)), 0, true, false, false, true)
	draw_lib.DrawRoundedBox(x + gap, y + (h / 2) + gap, math.min(VEL_ease:get().vel_w, w), thickness, color, 0, true, false, false, true)

	//Text
	SetFont("HUD_VEL", {
		font = "Gidole",
		weight = 600,
		size = 45
	})

	surface.SetTextColor( color )
	local velX, velY = surface.GetTextSize( math.Round( vel ) )

	draw_lib.Text(math.Round( vel ), x + (w / 2), y - 10, 2, 1, 0.9)
end

presets.create("vel_generic_1", SpeedDisplay)
