/*
	Creator     - Pivius/Moose
	Preset      - hp_generic_1
	Description -
		A HUD style by moose.
		Goes in style with keyboard_echoes preset.
	Benchmark in 1000 calls   -
		Doing nothing -
		Healing       -
*/

local global_vars = {}
local hp_color = util.palette.CARMINE
local base_color = util.palette.LIGHT_GREY
local dark = false

if dark then
	base_color = util.palette.DARK_GREY
end

local ease_module = load.Module( "modules/ease.lua" )
local easing = {a = 0, hp_w = 0}
local HP_ease = ease_module.new(1, table.Copy(easing), table.Copy(easing), "outCubic")
local HP_fade_ease = ease_module.new(1, {hp_w = 0}, {hp_w = 0}, "outCubic")
local HP_bar = Material(GAMEMODE_PATH .. "/UI/HP/Health_Bar.png", "noclamp")
local HP_bar_drain = Material(GAMEMODE_PATH .. "/UI/HP/Health_Bar_drain.png", "noclamp")

local function HealthDisplay(ply, scrw, scrh, self)
	local localPly = Spectate.IsSpectating(ply) or ply
	local w, h     = 300, 5                      // Width and Height
	local x, y     = -(w/2) + (scrw/2), scrh - 300 // X and Y position
	local hp      = localPly:Health()
	local alpha    = HP_ease:get().a
	local color    = ColorAlpha(hp_color, alpha)
	local shade    = Color(base_color.r/1.5, base_color.g/1.5, base_color.b/1.5)
	local shadow    = Color(shade.r/1.5, shade.g/1.5, shade.b/1.5)
	local hp_width = hp*(w/math.max(100, hp))
	local thickness = 25
	local gap = 2

	//Easing
	HP_ease:update(2.25)
	HP_ease:Target({["a"] = 255, ["hp_w"] = hp_width}, true)

	if HP_fade_ease:get().hp_w <= HP_ease:get().hp_w then
		HP_fade_ease:get().hp_w = hp_width
	end

	HP_fade_ease:update(1.5)
	HP_fade_ease:Target({["hp_w"] = hp_width}, true)

	// Draws the boxes
	draw_lib.DrawRoundedBox(x, y + (h / 2), w, thickness, ColorAlpha(base_color, math.max(alpha-10, 0)), 0, true, false, false, true)
	draw_lib.DrawRoundedBox(x - gap, y + (h / 2) + gap, w , thickness, ColorAlpha(shadow, math.max(alpha-10, 0)), 0, true, false, false, true)
	draw_lib.DrawRoundedBox(x - gap, y + (h / 2) + gap, math.min(HP_fade_ease:get().hp_w, w), thickness, ColorAlpha(color, math.Clamp(color.a - 200, 0 ,255)), 0, true, false, false, true)
	draw_lib.DrawRoundedBox(x - gap, y + (h / 2) + gap, math.min(HP_ease:get().hp_w, w), thickness, color, 0, true, false, false, true)

	//Text
	SetFont("HUD_HP", {
		font = "Gidole",
		weight = 600,
		size = 45
	})

	surface.SetTextColor( color )
	local hpX, hpY = surface.GetTextSize( math.Round( hp ) )
	
	draw_lib.Text(math.Round( hp ), x + (w / 2), y - 10, 2, 1, 0.9)
end

presets.create("hp_generic_1", HealthDisplay)
