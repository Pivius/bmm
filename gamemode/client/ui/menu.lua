local open = false


local SELECTION =
{
	Init = function( self )
		local ply = LocalPlayer()
		SetFont("MENU_BUTTON_TEXT", {
			font = "Gidole",
			size = 25
		})
		self.PrimaryColor = util.palette.LIGHT_GREY
		self.SecondaryColor = util.palette.ABSOLUTE_ZERO

		self.Frames = {}
		self.Primary = {}
		self.Secondary = {}
		self.Frames.LeftFrame = self:Add( "VGUI_BASE" )
		self.Frames.LeftFrame:SetPos( -250, 40 )
		self.Frames.LeftFrame:SetSize(ScrW()*0.25, ScrH() - 80)
		self.Frames.LeftFrame.Draw = false

		self.Primary.Profile = self.Frames.LeftFrame:Add( "VGUI_BASE" )
		self.Primary.Profile:SetPos( 0, 0 )
		self.Primary.Profile:SetSize(ScrW()*0.25, (ScrH() - 80)/3)
		self.Primary.Profile.Draw = true
		self.Primary.Profile:SetRounded(25)

	end,
	
	PerformLayout = function( self )
		self:SetSize( ScrW(), ScrH() )
		self:SetPos( 0, 0 )
	end,
	
	SetPrimary = function( self, color )
		self.PrimaryColor = color
		

		for _, ui in pairs(self.Primary) do
			ui:SetColor(color)

		end
	end,

	SetSecondary = function( self, color )
		self.SecondaryColor = color

		for _, ui in pairs(self.Secondary) do
			ui:SetColor(color)
		end
	end,

	Paint = function( self, w, h )

	end,
	
	Think = function( self, w, h )

	end
}

SELECTION = vgui.RegisterTable( SELECTION, "EditablePanel" );

if ( IsValid( Selection ) ) then
	Selection:Remove()
end

function GM:OnSpawnMenuOpen()
	
	gui.EnableScreenClicker(true)
	open = true
	
	if (not IsValid( Selection ) ) then
		Selection = vgui.CreateFromTable( SELECTION )
		Selection:SetAlpha(0)
	end

	if IsValid( Selection ) then
		Selection:AlphaTo(255, 0.15)
		Selection:Show()
		Selection:MakePopup()
		Selection:SetKeyboardInputEnabled( false )
		Selection:SetPrimary(util.palette.DARK_GREY)
		Selection.Frames.LeftFrame:AnimateTo('outCubic', 0.15, {x = 40} )
		
	end
end

function GM:OnSpawnMenuClose()
	open = false
	
	if ( IsValid( Selection ) ) then
		timer.Create("vgui_selection", 0.5, 1, function()
			if not open then
				Selection:Remove()
			end
		end)

		for _, ui in pairs(Selection.Frames) do
			Selection:AlphaTo(0, 0.15)
			Selection.Frames.LeftFrame:AnimateTo('inOutCubic', 0.25, {x = -250} )
		end
	end
	gui.EnableScreenClicker(false)
end
