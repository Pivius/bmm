vgui_ease =  load.Module( "modules/ease.lua" )
local BASE =
{
    PreInit = function(self)
        self.Color = util.palette.WHITE
        self.Draw = false
        self.Ease = vgui_ease.new(1, {x = 0, y = 0, w = 128, h = 128, color = {self.Color:Unpack()}}, {}, "linear")
        self.Rounded = 0
    end,
	Init = function( self )
        self:PreInit()
    end,

    SetColor = function( self, col )
        self.Color = col
    end,

    GetColor = function( self )
        return self.Color
    end,

    SetDraw = function( self, bool )
        self.Draw = bool
    end,

    GetDraw = function( self )
        return self.Draw
    end,

    SetRounded = function( self, num )
        self.Rounded = num
    end,

    GetRounded = function( self )
        return self.Rounded
    end,

    PerformLayout = function( self, w, h )
    end,

    -- {x = 0, y = 0, w = 0, h = 0, color = 0}
    AnimateTo = function( self, ease, dt, output )
        local x, y = self:GetPos()
        local w, h = self:GetSize()
        if output.color then
            if IsColor(output.color) then
                output.color = {output.color:Unpack()}
            end
        end
        
        self.Ease = vgui_ease.new(dt, {["x"] = x, ["y"] = y, ["w"] = w, ["h"] = h, color = {self.Color:Unpack()}}, output, ease)
    end,

    Paint = function( self, w, h )
        if self.Draw then
            draw_lib.DrawRoundedBox(0, 0, w, h, self.Color, self.Rounded, true, true, true, true)
        end
	end,
    
    EaseThink = function(self)
        if self.Ease.time < 1 then
            local init_x, init_y = self:GetPos()
            local init_w, init_h = self:GetSize()
            
            x = (self.Ease.target.x and self.Ease:get().x) or init_x
            y = (self.Ease.target.y and self.Ease:get().y) or init_y
            w = (self.Ease.target.w and self.Ease:get().w) or init_w
            h = (self.Ease.target.h and self.Ease:get().h) or init_h
            col = (self.Ease.target.color and self.Ease:get().color) or self:GetColor()

            self.Ease:update(1)

            if self.Ease.target.x or self.Ease.target.y then
                self:SetPos( x, y )
            end

            if self.Ease.target.w then
                self:SetWide( w )
            end

            if self.Ease.target.h then 
                self:SetTall( h )
            end

            if self.Ease.target.color then 
                self:SetColor(util.color.Pack(col))
            end
        end
    end,

    Think = function( self )
        self:EaseThink()
	end
}
vgui.Register( "VGUI_BASE", BASE, "EditablePanel" )

local BASE_IMAGE =
{
    Init = function( self )
        self:PreInit()
        self.Material = Material(GAMEMODE_PATH .. "/white.png", "noclamp")
        self.uv = {0, 0, 1, 1}
    end,

    SetMaterial = function(self, material)
        self.Material = Material(GAMEMODE_PATH .. material, "noclamp")
    end,
    
    Paint = function( self, w, h )
        if self.Draw then
            local uv = self.uv
            draw_lib.DrawBoxTextured( self.Material, 0, 0, w, h, uv[1], uv[2], uv[3], uv[4] )
        end
    end,
    
    Think = function( self )
        self:EaseThink()
	end
}

vgui.Register( "VGUI_BASE_IMAGE", BASE_IMAGE, "VGUI_BASE" )

local BASE_BUTTON =
{
	
    Init = function( self )
        self:PreInit()
        self.Button = self:Add( "DButton" )
        self.Button:SetPos( 0, 0 )
        self.Button:SetSize( 128, 128 )
        self.Button:SetText("")
        self.Button.Paint = function()

        end

        self.Button.DoClick = function()
            self:DoClick()
        end

        self.Button.OnCursorEntered = function()
            self:OnCursorEntered()
        end

        self.Button.OnCursorExited = function()
            self:OnCursorExited()
        end
    end,
    
    PerformLayout = function( self, w, h )
        self.Button:SetSize( w, h )
    end,

    DoClick = function()

    end,

    OnCursorEntered = function()

    end,

    OnCursorExited = function()

    end,
    
    Think = function( self )
        self:EaseThink()
	end
}

vgui.Register( "VGUI_BASE_BUTTON", BASE_BUTTON, "VGUI_BASE" )
