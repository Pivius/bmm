local LIST = {}

function LIST:Init()
	local x, y = self:GetPos()
	
	local w = self:GetWide()
	local h = self:GetTall()

	self.PanelColor = Color(15,15,15,200)

	self.Panel= self:Add( "DFrame" )
	self.Panel:SetPos( 0, 0 )
	self.Panel:SetSize(w, h)
	self.Panel:SetTitle("")
	self.Panel:SetDraggable(false)
	self.Panel:ShowCloseButton( false )

	self.Panel.Paint = function(s, w, h )
		surface.SetDrawColor( self.PanelColor )
		surface.DrawRect(0, 0, w, h)
	end
	
	self.Bar = self:Add( "EditablePanel" )
	self.Bar:SetPos( 0, 0 )
	self.Bar:SetSize(600, 40)
	self.Bar.Buttons = {}
	self.CurCol = Color(255,255,255)
	
	self.List = vgui.Create("DScrollPanel", self.Panel)
	
	self.List:SetSize(w - 20, h - 20)
	self.List:SetPos(10,10)
	self.List.Paint = function(self, w, h )

	end
	
	local sbar = self.List:GetVBar()
	function sbar:Paint( w, h )
		surface.SetDrawColor( Color(25,25,25,200) )
		surface.DrawRect(w/2, 0, w/2, h)
	end
	function sbar.btnUp:Paint( w, h )
		surface.SetDrawColor( Color(100,100,100,100) )
		surface.DrawRect(w/2, 0, w/2, h)
	end
	function sbar.btnDown:Paint( w, h )
		surface.SetDrawColor( Color(100,100,100,100) )
		surface.DrawRect(w/2, 0, w/2, h)
	end
	function sbar.btnGrip:Paint( w, h )
		surface.SetDrawColor( Color(50,50,50,200) )
		surface.DrawRect(w/2, 0, w/2, h)
	end
	
	self.List.OnVScroll = function(self, iOffset )
		self.offset = iOffset
	end
	self.List.offsetsmooth = 0
	self.List.vBarSmooth = 0
	self.List.offset = 0
	self.List.Think = function(self)
		self.offsetsmooth = self.offsetsmooth and Lerp(0.1, self.offsetsmooth , self.offset) or self.offset
		self.vBarSmooth = self.vBarSmooth and Lerp(0.2, self.vBarSmooth , self.offset) or self.offset
		self:GetVBar().Scroll = -(self.vBarSmooth)
		--self.VBar:SetPos(0,0)
		self.pnlCanvas:SetPos( 0, self.offsetsmooth )
	end
	
	self.List.PerformLayout = function(self)
		local wide = self:GetWide()
		self:Rebuild()
		self.VBar:SetUp( self:GetTall()-20, self.pnlCanvas:GetTall() )
		--self.VBar:SetSize(0,0)

		self.pnlCanvas:SetWide(wide)
		self:Rebuild()
	end
	
end

function LIST:Size(w, h)
	self:SetSize(w, h)
	self.Panel:SetSize(w, h)
	self.List:SetSize(w, h)
	self.List:PerformLayout()
end

vgui.Register( "Custom_List", LIST, "EditablePanel" )