load.AddModule( "libraries/swl/lexer.lua" )
local lexer = load.Module( "libraries/swl/lexer.lua" )

local test = file.Read("swl/test.txt", "DATA")

local function doMath(tbl)
    local num = 0
    local operator = "+"
    for i = 1, #tbl do
        if tbl[i].type == "number" then
            if operator == "+" then
                num = num + tonumber(tbl[i].data)
            elseif operator == "-" then
                num = num - tonumber(tbl[i].data)
            elseif operator == "*" then
                num = num * tonumber(tbl[i].data)
            elseif operator == "/" then
                num = num / tonumber(tbl[i].data)
            elseif operator == "^" then
                num = num ^ tonumber(tbl[i].data)
            elseif operator == "%" then
                num = num % tonumber(tbl[i].data)
            end
        elseif tbl[i].type == "operator" then
            operator = tbl[i].data
        end
    end

    return num
end

local function mathLex(lexer)
    local segments = {}
    local prevToken = ""

    for k, line in pairs(lexer) do
        for _, tokens in pairs(line) do
            if tokens.type == "number" then
                if prevToken != "operator" then
                    segments[#segments+1] = {}
                end

                table.insert(segments[#segments], tokens)
            elseif tokens.type == "operator" and prevToken == "number" then
                table.insert(segments[#segments], tokens)
            end

            if tokens.type != "whitespace" then
                prevToken = tokens.type
            end
        end
    end

    if #segments > 0 then
        print(doMath(segments[1]))
    end
end
--mathLex(lexer(test))
--PrintTable(lexer(test))