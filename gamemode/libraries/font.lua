font = {}
font.fonts = {}
font.clientList = {

}
font.list = {

}
font.setFont = "Trebuchet24"

-- Use to init fonts.
function InitalizeFonts()
end


function SetFont(unique, tbl)
  if not tbl then
    if font.fonts[unique] then
      local font_name = unique .. "_" .. font.fonts[unique].last_size
      if font.setFont ~= font_name then
        surface.SetFont(font_name)
        font.setFont = font_name
        return font_name
      end
    end
  else
    if font.fonts[unique] && font.fonts[unique][tbl.size] then
      font.fonts[unique].last_size = tbl.size
      local font_name = unique .. "_" .. tbl.size
      font.setFont = font_name
      surface.SetFont(font_name)
      return font_name
    else
      if !font.fonts[unique] then
        font.fonts[unique] = {}
      end
      font.fonts[unique][tbl.size] = tbl
      font.fonts[unique].last_size = tbl.size
      local font_name = unique .. "_" .. font.fonts[unique].last_size
      font.setFont = font_name
      surface.CreateFont(font_name, tbl)
      surface.SetFont(font_name)
      return font_name
    end
  end
end

function GetFont(unique, size)
  if istable(unique) then
    print(unique)
  end
  local t = string.Explode( "_", unique )

  size = tonumber(table.remove(t))
  
  if size then
    unique = table.concat(t, "_")
  end

  if font.fonts[unique] then
    
    if font.fonts[unique][font.fonts[unique][size]] then
      
      return unique .. "_" .. size
    end
    if font.fonts[unique][font.fonts[unique].last_size] then
      return unique .. "_" .. font.fonts[unique].last_size
    end
  end
  return "Trebuchet24"
end
