Admin.Commands:createCMD("incognito", "caller, target", function(caller, target)
	-- Check if the caller has chosen a target
	if target then
    if !Admin:IsHigherThan( caller, Admin.Player:GetRank(target:SteamID()) ) then
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You can't target this user!")
      return
    end
    if Spectate.Players[target]["Incognito"] then
      Spectate.Players[target]["Incognito"] = false
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["VGAM"], target:Nick(), Admin.Colors["normal"], " is no longer in incognito mode.")
      chat.Text(target, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You are no longer in incognito mode, your name is shown when you spectate.")
    else
      Spectate.Players[target]["Incognito"] = true
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["VGAM"], target:Nick(), Admin.Colors["normal"], " is now in incognito mode.")
      chat.Text(target, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You are now in incognito mode, your name is not shown when you spectate.")
    end

  else
    if Spectate.Players[caller]["Incognito"] then
      Spectate.Players[caller]["Incognito"] = false
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You are no longer in incognito mode, your name is shown when you spectate.")
    else
      Spectate.Players[caller]["Incognito"] = true
      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], "You are now in incognito mode, your name is not shown when you spectate.")
    end
	end


end)

Admin.Commands:createCMD("force_spectate", "caller, target", function(caller, target)
	-- Check if the caller has chosen a target
	if target then
		if !plyFromNick(target):IsBot() then
	    if !Admin:IsHigherThan( caller, Admin.Player:GetRank(target:SteamID()) ) then
	      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You can't target this user!")
	      return
	    end
			Spectate.StartSpec(plyFromNick(target), caller)
		else
			Spectate.StartSpec(plyFromNick(target), caller)
		end
	end

end)

Admin.Commands:createCMD("force_unspectate", "caller, target", function(caller, target)
	-- Check if the caller has chosen a target
	if target then
		if !plyFromNick(target):IsBot() then
	    if !Admin:IsHigherThan( caller, Admin.Player:GetRank(target:SteamID()) ) then
	      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["error"], "You can't target this user!")
	      return
	    end
			Spectate.EndSpec(plyFromNick(target), caller)
		else
			Spectate.EndSpec(plyFromNick(target), caller)
		end
	end

end)
