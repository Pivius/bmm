// Commands that users should be able to do
--Admin:AddCmd("user", "ranks")
Admin.Commands:createCMD("ranks", "caller", function(caller)
  local rank = calcRank(caller:Points())
	local pts = caller:Points()
  local rankCol = rankColor(rank)
  local nextRankPos = 1
  for _, v in pairs(getRanks()) do
    chat.Text(caller, v[2], v[1], Admin.Colors["normal"], " - ", calcRankPts(v[1]))
    if v[1] == rank then
      nextRankPos = math.min( _+1, #getRanks() )
    end
  end
  chat.Text(caller, Admin.Colors["normal"], 'Points: ', pts)
  chat.Text(caller, Admin.Colors["normal"], 'Current Rank: ', rankCol, rank)
  chat.Text(
    caller,
    Admin.Colors["normal"],
    'Next Rank: ',
    getRanks()[nextRankPos][2],
    getRanks()[nextRankPos][1],
    Admin.Colors["normal"],
    " - ",
    calcRankPts(getRanks()[nextRankPos][1])
  )
end)

Admin.Commands:createCMD("setpoints", "caller, target, points / caller, points", function(caller, ...)
  local args = {...}
  local pts = tonumber(args[1])
  local ply = Admin.Player:GetTarget(caller, args[1])
  if !ply && pts then
    ply = caller
  end

  if ply then
    if pts || ply:SteamID() == caller:SteamID() then
      if ply:SteamID() == caller:SteamID() && #args > 1 then
        pts = tonumber(args[2])
      end

      chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], 'You set your points to ', pts, ".")
      givePts(caller, pts)
    elseif type(args[1]) == "string" && ply:SteamID() != caller:SteamID() then
      pts = tonumber(args[2])
      local nick = ply:Nick()
      if pts then
        // Message caller
        chat.Text(caller, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], 'You set ', Admin.Colors["VGAM"], nick, Admin.Colors["normal"], ' points to ', pts, ".")
        // Message target
        chat.Text(ply, Admin.Colors["VGAM"], "[VGAM] ", Admin.Colors["normal"], 'Your points was set to ' , pts, " by ", Admin.Colors["VGAM"], caller:Nick())
        givePts(ply, pts)
      end
    end
  end

end)
