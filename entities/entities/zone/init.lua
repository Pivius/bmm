AddCSLuaFile('cl_init.lua')
AddCSLuaFile('shared.lua')

include('shared.lua')
function ENT:Initialize()
	self:PhysicsInitBox( self.min, self.max )
	local BBOX = (self.max - self.min)
	self:SetSolid( SOLID_BBOX )
	self:SetCollisionBounds( self.min, self.max )
	--self:SetPos(self.min)
	self:SetTrigger( true )
	self:DrawShadow( false )
	self:SetNotSolid( true )
	self:SetNoDraw( false )
	self:AddEFlags( EFL_FORCE_CHECK_TRANSMIT )
	if self.Phys and self.Phys:IsValid() then
		self.Phys:Sleep()
		self.Phys:EnableCollisions( false )
	end
	self:SetID( self.id )
	self:SetType( self.type )
end

function ENT:StartTouch( ent )
	if IsValid( ent ) and ent:IsPlayer() and ent:Team() != TEAM_SPECTATOR then
		hook.Call("Zone_Entered", nil, ent, self:GetID(), self:GetType())
	end
end

function ENT:EndTouch( ent )
	if IsValid( ent ) and ent:IsPlayer() and ent:Team() != TEAM_SPECTATOR then
		hook.Call("Zone_Left", nil, ent, self:GetID(), self:GetType())
	end
end

function ENT:UpdateTransmitState()
	return TRANSMIT_ALWAYS
end
